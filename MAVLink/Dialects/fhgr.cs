﻿// MIT License
//
// Copyright (c) 2018 Alexey (https://github.com/asvol)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// This code was generate by tool Asv.Mavlink.Shell version 3.1.0

using System;
using System.Text;
using Asv.Mavlink.V2.Common;
using Asv.IO;

namespace Asv.Mavlink.V2.Fhgr
{

    public static class FhgrHelper
    {
        public static void RegisterFhgrDialect(this IPacketDecoder<IPacketV2<IPayload>> src)
        {
            src.Register(() => new RadiationDetectorDataPacket());
            src.Register(() => new ChemicalDetectorDataPacket());
        }
    }

    #region Enums


    #endregion

    #region Messages

    /// <summary>
    /// Radiation Detector Data
    ///  RADIATION_DETECTOR_DATA
    /// </summary>
    public class RadiationDetectorDataPacket : PacketV2<RadiationDetectorDataPayload>
    {
        public const int PacketMessageId = 203;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 227;
        public override bool WrapToV2Extension => false;

        public override RadiationDetectorDataPayload Payload { get; } = new RadiationDetectorDataPayload();

        public override string Name => "RADIATION_DETECTOR_DATA";
    }

    /// <summary>
    ///  RADIATION_DETECTOR_DATA
    /// </summary>
    public class RadiationDetectorDataPayload : IPayload
    {
        public byte GetMaxByteSize() => 24; // Sum of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 24; // of byte sized of fields (exclude extended)
        public byte GetCurrentByteSize()
        {
            var sum = 0;
            sum += 4; //DetectorTimeMs
            sum += 4; //DeltaTS
            sum += 4; //Counts0
            sum += 4; //Counts1
            sum += 4; //Rates0
            sum += 4; //Rates1
            return (byte)sum;
        }



        public void Deserialize(ref ReadOnlySpan<byte> buffer)
        {
            DetectorTimeMs = BinSerialize.ReadUInt(ref buffer);
            DeltaTS = BinSerialize.ReadFloat(ref buffer);
            Counts0 = BinSerialize.ReadUInt(ref buffer);
            Counts1 = BinSerialize.ReadUInt(ref buffer);
            Rates0 = BinSerialize.ReadFloat(ref buffer);
            Rates1 = BinSerialize.ReadFloat(ref buffer);

        }

        public void Serialize(ref Span<byte> buffer)
        {
            BinSerialize.WriteUInt(ref buffer, DetectorTimeMs);
            BinSerialize.WriteFloat(ref buffer, DeltaTS);
            BinSerialize.WriteUInt(ref buffer, Counts0);
            BinSerialize.WriteUInt(ref buffer, Counts1);
            BinSerialize.WriteFloat(ref buffer, Rates0);
            BinSerialize.WriteFloat(ref buffer, Rates1);
            /* PayloadByteSize = 24 */
            ;
        }





        /// <summary>
        /// Time since system boot
        /// OriginName: detector_time_ms, Units: ms, IsExtended: false
        /// </summary>
        public uint DetectorTimeMs { get; set; }
        /// <summary>
        /// The time that has passed since the last measurement has been sent.
        /// OriginName: delta_t_s, Units: s, IsExtended: false
        /// </summary>
        public float DeltaTS { get; set; }
        /// <summary>
        /// An array of detector counts (accumulated since start). First element stands for all the counts from low to mid cut.
        /// OriginName: counts_0, Units: , IsExtended: false
        /// </summary>
        public uint Counts0 { get; set; }
        /// <summary>
        /// Second is for the values that are in the bins from mid to high cut.
        /// OriginName: counts_1, Units: , IsExtended: false
        /// </summary>
        public uint Counts1 { get; set; }
        /// <summary>
        /// An array containing the rate below and above the mid cut. The rate unit is counts per second normalized from the counted neutrons in the time difference delta_t_s
        /// OriginName: rates_0, Units: counts/s, IsExtended: false
        /// </summary>
        public float Rates0 { get; set; }
        /// <summary>
        /// An array containing the rate below and above the mid cut. The rate unit is counts per second normalized from the counted neutrons in the time difference delta_t_s
        /// OriginName: rates_1, Units: counts/s, IsExtended: false
        /// </summary>
        public float Rates1 { get; set; }
    }
    /// <summary>
    /// Chemical Detector Data
    ///  CHEMICAL_DETECTOR_DATA
    /// </summary>
    public class ChemicalDetectorDataPacket : PacketV2<ChemicalDetectorDataPayload>
    {
        public const int PacketMessageId = 202;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 211;
        public override bool WrapToV2Extension => false;

        public override ChemicalDetectorDataPayload Payload { get; } = new ChemicalDetectorDataPayload();

        public override string Name => "CHEMICAL_DETECTOR_DATA";
    }

    /// <summary>
    ///  CHEMICAL_DETECTOR_DATA
    /// </summary>
    public class ChemicalDetectorDataPayload : IPayload
    {
        public byte GetMaxByteSize() => 16; // Sum of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 16; // of byte sized of fields (exclude extended)
        public byte GetCurrentByteSize()
        {
            var sum = 0;
            sum += 4; //Concentration
            sum += 4; //Dose
            sum += 2; //AgentId
            sum += 2; //Bars
            sum += 2; //BarsPeak
            sum += 2; //HazardLevel
            return (byte)sum;
        }



        public void Deserialize(ref ReadOnlySpan<byte> buffer)
        {
            Concentration = BinSerialize.ReadFloat(ref buffer);
            Dose = BinSerialize.ReadFloat(ref buffer);
            AgentId = BinSerialize.ReadUShort(ref buffer);
            Bars = BinSerialize.ReadUShort(ref buffer);
            BarsPeak = BinSerialize.ReadUShort(ref buffer);
            HazardLevel = BinSerialize.ReadUShort(ref buffer);

        }

        public void Serialize(ref Span<byte> buffer)
        {
            BinSerialize.WriteFloat(ref buffer, Concentration);
            BinSerialize.WriteFloat(ref buffer, Dose);
            BinSerialize.WriteUShort(ref buffer, AgentId);
            BinSerialize.WriteUShort(ref buffer, Bars);
            BinSerialize.WriteUShort(ref buffer, BarsPeak);
            BinSerialize.WriteUShort(ref buffer, HazardLevel);
            /* PayloadByteSize = 16 */
            ;
        }





        /// <summary>
        /// Concentration (mg/m3), IEEE floating point format
        /// OriginName: concentration, Units: mg/m3, IsExtended: false
        /// </summary>
        public float Concentration { get; set; }
        /// <summary>
        /// Dose (mg-min/m3), IEEE floating point format
        /// OriginName: dose, Units: mg-min/m3, IsExtended: false
        /// </summary>
        public float Dose { get; set; }
        /// <summary>
        /// Agent ID, 0=No agent, 1=GA, 2=GB, 3=GD/GF, 4=VX, 5=VXR, 6=DPM, 7=AC/CK, 8=CK, 9=AC, 11=HD
        /// OriginName: agent_id, Units: , IsExtended: false
        /// </summary>
        public ushort AgentId { get; set; }
        /// <summary>
        /// Bars (0 - 8)
        /// OriginName: bars, Units: , IsExtended: false
        /// </summary>
        public ushort Bars { get; set; }
        /// <summary>
        /// Peak Bars (0 - 8)
        /// OriginName: bars_peak, Units: , IsExtended: false
        /// </summary>
        public ushort BarsPeak { get; set; }
        /// <summary>
        /// Hazard Level - none, low, medium, high ( 0-3 )
        /// OriginName: hazard_level, Units: , IsExtended: false
        /// </summary>
        public ushort HazardLevel { get; set; }
    }


    #endregion


}
