﻿using Asv.Mavlink;
using Asv.Mavlink.V2.Fhgr;
using Mav2MQTT;
using Microsoft.Extensions.Configuration;
using MQTTnet;
using MQTTnet.Client;

var configuration = new ConfigurationBuilder().AddCommandLine(args).AddEnvironmentVariables().Build();
var mavlinkConnectionStrings = configuration.GetValue("mavlinkConnectionStrings", "udp://0.0.0.0:14540");
var mqttBrokerAddress = configuration.GetValue("mqttBrokerAddress", "192.168.1.24");
var mqttBrokerPort = configuration.GetValue("mqttBrokerPort", 1883);

if (string.IsNullOrEmpty(mavlinkConnectionStrings))
{
    Console.WriteLine("Required parameter 'mavlinkConnectionStrings' not specified!");
    return;
}

if (string.IsNullOrEmpty(mqttBrokerAddress))
{
    Console.WriteLine("Required parameter 'mqttBrokerAddress' not specified!");
    return;
}

if (mqttBrokerPort <= 0)
{
    Console.WriteLine("Required parameter 'mqttBrokerPort' not specified!");
    return;
}

var mqttFactory = new MqttFactory();
var mqttClient = mqttFactory.CreateMqttClient();
var mqttClientOptions = new MqttClientOptionsBuilder().WithTcpServer(mqttBrokerAddress, mqttBrokerPort).WithTimeout(TimeSpan.FromSeconds(10)).Build();
var connectionResult = await mqttClient.ConnectAsync(mqttClientOptions, CancellationToken.None);

if (connectionResult.ResultCode != MqttClientConnectResultCode.Success)
{
    Console.WriteLine($"Connection to MQTT Broker failed with status {connectionResult.ResultCode}");
    return;
}

var mavlinkConnections = mavlinkConnectionStrings.Split(";")
    .Select(p => new MavlinkV2Connection(p, r =>
    {
        MavlinkV2Connection.RegisterDefaultDialects(r);
        r.RegisterFhgrDialect();
    }))
    .ToList();

foreach (var connection in mavlinkConnections)
{
    var connectionId = connection.DataStream.Name.Split(":")[1];

    // special case if dynamic port with rhost and rport
    if (connectionId.Contains("=>"))
    {
        connectionId = connectionId.Split("=>")[0];
    }

    Console.WriteLine($"Creating MQTTWriter for connection {connection.DataStream.Name}");

    var writer = new MQTTWriter(connectionId, mqttClient, configuration);
    connection.Subscribe(writer);
}

while (true)
{
    await Task.Delay(TimeSpan.FromSeconds(1));
}
