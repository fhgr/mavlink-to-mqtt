﻿using Asv.Mavlink.V2.Common;
using Asv.Mavlink;
using MQTTnet.Client;
using MQTTnet;
using System.Text.Json.Serialization;
using System.Text.Json;
using Asv.Mavlink.V2.Fhgr;
using System.Diagnostics;
using System.Reactive.Concurrency;
using Microsoft.Extensions.Configuration;

namespace Mav2MQTT
{
    public class MQTTWriter : IObserver<IPacketV2<IPayload>>
    {
        private readonly string _vehicleId;
        private readonly IMqttClient _mqttClient;
        private readonly MqttApplicationMessageBuilder _mqttApplicationMessageBuilder;
        private readonly JsonSerializerOptions _jsonSerializerOptions;
        private bool _runOnce = true;
        private Stopwatch _stopwatch = new();
        private bool _clearOnce = true;
        private readonly List<double> _deltas =  new();
        private readonly List<double> _anomalies = new();
        private readonly bool _enableDebugLogging;

        public MQTTWriter(string vehicleId, IMqttClient mqttClient, IConfigurationRoot configuration)
        {
            _vehicleId = vehicleId;
            _mqttClient = mqttClient;
            _enableDebugLogging = configuration.GetValue("enableDebugLogging", false);
            _mqttApplicationMessageBuilder = new MqttApplicationMessageBuilder();
            _jsonSerializerOptions = new JsonSerializerOptions
            {
                NumberHandling = JsonNumberHandling.AllowReadingFromString | JsonNumberHandling.AllowNamedFloatingPointLiterals
            };
        }

        public void OnCompleted() { }

        public void OnError(Exception error)
        {
            Console.WriteLine($"{_vehicleId}: {error.Message}");
        }

        public async void OnNext(IPacketV2<IPayload> packet)
        {
            // Discard Odometery, Ping and Timesync and Heartbeat Packets for now
            if (packet is OdometryPacket || packet is PingPacket || packet is TimesyncPacket || packet is HeartbeatPacket)
            {
                return;
            }

            if (_enableDebugLogging && packet is DistanceSensorPacket)
            {
                if (_runOnce)
                {
                    _runOnce = false;
                    _stopwatch = new Stopwatch();
                    _stopwatch.Start();
                }
                else
                {
                    var delta = _stopwatch.Elapsed.TotalMilliseconds;
                    _deltas.Add(delta);
                    _stopwatch.Restart();
                    Console.WriteLine($"Cur {delta} Min {_deltas.Min()}, Max {_deltas.Max()} Avg {_deltas.Average()} Anomalies {_anomalies.Count()}");


                    if(delta >= 1000)
                    {
                        _anomalies.Add(delta);
                    }

                    if (_deltas.Count > 50 && _clearOnce)
                    {
                        _clearOnce = false;
                        _anomalies.Clear();
                        Console.WriteLine("clea");
                        _deltas.Clear();
                    }
                }
            }

            var serializedPayload = JsonSerializer.SerializeToUtf8Bytes(packet.Payload, packet.Payload.GetType(), _jsonSerializerOptions);
            var applicationMessage = _mqttApplicationMessageBuilder
                .WithTopic($"{_vehicleId}/{packet.Name}")
                .WithPayload(serializedPayload)
                .Build();

            await _mqttClient.PublishAsync(applicationMessage, CancellationToken.None);
        }
    }

}
